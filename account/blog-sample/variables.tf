variable "az_subscription_id" {
  sensitive = true
  nullable = true
}

variable "az_tenant_id" {
  sensitive = true
  nullable = true
}

variable "az_client_secret" {
  sensitive = true
  nullable = true
}

variable "gcp_svc_key" {
  sensitive = true
  nullable = true
}

variable "gcp_project" {
  sensitive = true
  nullable = true
}

variable "gcp_region" {
  nullable = true
}
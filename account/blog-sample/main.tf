module "aws" {
  source = "../../modules/aws"
  
}

module "azure" {
  source = "../../modules/azure"
  az_client_secret = var.az_client_secret
  az_subscription_id = var.az_subscription_id
  az_tenant_id = var.az_tenant_id
}

module "gcp" {
  source = "../../modules/gcp"
  gcp_svc_key = var.gcp_svc_key
  gcp_project = var.gcp_project
  gcp_region = var.gcp_region
}
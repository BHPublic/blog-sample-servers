provider "aws" {
  region = "us-west-1"
  alias = "aws"

}

provider "azurerm" {
  features {}
  alias = "az"
  subscription_id = var.az_subscription_id
  tenant_id = var.az_tenant_id
  client_secret = var.az_client_secret
}

provider "google" {
  region = "us-west-1"
  alias = "gcp"
  
}
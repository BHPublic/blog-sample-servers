resource "azurerm_resource_group" "blog-sample-rg" {
  name     = "blog-sample-rg"
  location = "West US"
}

resource "azurerm_virtual_network" "blog-sample-network" {
  name                = "blog-sample-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.blog-sample-rg.location
  resource_group_name = azurerm_resource_group.blog-sample-rg.name
}

resource "azurerm_subnet" "blog-sample-subnet" {
  name                 = "blog-sample-subnet"
  resource_group_name  = azurerm_resource_group.blog-sample-rg.name
  virtual_network_name = azurerm_virtual_network.blog-sample-network.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_public_ip" "blog-sample-public-ip" {
  name                = "blog-sample-public-ip"
  location            = azurerm_resource_group.blog-sample-rg.location
  resource_group_name = azurerm_resource_group.blog-sample-rg.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "blog-sample-nic" {
  name                = "blog-sample-nic"
  location            = azurerm_resource_group.blog-sample-rg.location
  resource_group_name = azurerm_resource_group.blog-sample-rg.name

  ip_configuration {
    name                          = "Internal"
    subnet_id                     = azurerm_subnet.blog-sample-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.blog-sample-public-ip.id
  }
}

resource "azurerm_linux_virtual_machine" "blog-sample-vm" {
  name                = "blog-sample-vm"
  location            = azurerm_resource_group.blog-sample-rg.location
  resource_group_name = azurerm_resource_group.blog-sample-rg.name
  size                = "Standard_B1s"

  admin_username      = "testtest"
  network_interface_ids = [azurerm_network_interface.blog-sample-nic.id]

  admin_ssh_key {
       # make var
    username   = "testtest"
    public_key = file("~/keys/testtest.pub")  # Path to your SSH public key file
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
}
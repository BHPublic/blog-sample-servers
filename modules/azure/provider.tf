terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.91.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = var.az_subscription_id
  tenant_id = var.az_tenant_id
  client_secret = var.az_client_secret
  
}
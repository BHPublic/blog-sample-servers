variable "az_subscription_id" {
  sensitive = true
  nullable = true
}

variable "az_tenant_id" {
  sensitive = true
  nullable = true
}

variable "az_client_secret" {
  sensitive = true
  nullable = true
}
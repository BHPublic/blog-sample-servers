resource "google_compute_instance" "instance-blog-sample-bh" {
  name         = "instance-blog-sample-bh"
  machine_type = "e2-micro"
  zone = "us-west2-a"

  can_ip_forward      = false
  deletion_protection = false
  enable_display      = false

  boot_disk {
    auto_delete = true

    initialize_params {
      image = "projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240208"
      size  = 10
      type  = "pd-standard"
    }

  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  metadata = {
    foo = "bar"
  }

  service_account {
    # Make a var
    email  = "test-devops@iam.gserviceaccount.com"
    scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }

  shielded_instance_config {
    enable_integrity_monitoring = true
    enable_secure_boot          = false
    enable_vtpm                 = true
  }
}

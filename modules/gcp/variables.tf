variable "gcp_svc_key" {
  sensitive = true
  nullable = true
}

variable "gcp_project" {
  sensitive = true
  nullable = true
}

variable "gcp_region" {
  nullable = true
}
   # make var
resource "aws_instance" "blog-sample" {
  ami           = "ami-0ce2cb35386fc22e9"  # Replace with your desired AMI ID
  instance_type = "t2.micro"               # Replace with your desired instance type
  key_name      = "devops"         # Replace with the name of your existing SSH key pair
  tags = {
    Name = "blog-sample"  # Replace with your desired name for the instance
  }
}
